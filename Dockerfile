FROM php:7.4-fpm
LABEL Maintainer="Luis Lopes <luis@endressdigital.com>"

RUN apt-get update \
  && apt-get install -y \
    cron \
    nginx \
    libpng-dev \
    libicu-dev \
    libxslt1-dev \
    libssl-dev \
    libzip-dev \
    supervisor \
    libcurl4-gnutls-dev

RUN docker-php-ext-install \
  bcmath \
  gd \
  intl \
  pdo_mysql \
  xsl \
  zip \
  opcache

RUN docker-php-ext-install mysqli

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]